# Turris Omnia

In this document, I’m walking through a home network reconfiguration that involves reconfiguring an ISP-rented router
(in my case it is a UPC Cisco EP3925 wireless modem and router combo which I’ll just call *the Cisco*)
to act as a modem and installing Turris Omnia (which I’ll call just *the Omnia*) as an owned, reliable,
secure and hackable alternative.

[ ] Add a fun fact about the UPC wireless default passwords being derivable from the SSIDs

The Cisco is repurposed instead of purchasing a standalone modem because it is already there
and also because I have zero experience with configuring DOCSIS which is something I think
I would have needed to do if I decide to also buy my own modem.

That being said, Arris SurfBoard line DOCSIS 3.0 cable modems on Amazon appear as a viable option
(assuming they support EuroDocsis) and maybe a future project will involve introducing one of those guys
as a replacement for the Cisco. I especially like that they just accept the coaxial cable from the antenna
and provide a single Ethernet port, no bullshit.

## Factory resetting the Omnia

I have to do this step because immediately after unpacking the Omnia I connected it
to the Cisco and played around with it, configuring and exploring it.

I want to start with a clean slate for the actual installation, and to ensure that to be the case,
I researched how to factory reset the Omnia.

To do that, the *Reset* button on the backside of the Omnia is held and the frontside LEDs
going from green to red one after another until three of them are lit (power, `0` and `1`) are observed.
At that point, the *Reset* button is let go of.

This weird little counter system tells the Omnia I waited for 3 LEDs and thus want it to reset to the factory settings.
There are other levels of resetting the router which you can learn about on the official Turris site:

https://www.turris.cz/doc/en/howto/omnia_factory_reset

This gives me the out of the box experience with the Omnia once again. But before I proceed with the Omnia any further,
it’s time to take a look at the Cisco, and how to strip it to the bare minimum I need it to do.

## Configuring the Omnia

I’m going to configure the Omnia first, in part. I want the Omnia to be connected to the Cisco using a static IP,
that part will be postponed to the very beginning though. For now, the Omnia will be connected using DHCP
and I’ll set up a wireless network on it.

[ ] Change the surrounding wording to reflect this is done first

I’ll walk through the Omnia configuration in detail and comment on each setting
explaining what it does and why I configured the value the way I did.

## Modeming the Cisco

I’ll walk through the Cisco administrative interface, commenting on each setting,
how it affects the setup and most importantly whether it can be turned off.
Before I could do that, though, I have to resolve an issue with not being able
to access the administrative interface (and the Internet at large) while
connected to the Cisco by a network table, only while connected via the WiFi.

The culprit is my wired network interface settings, where I have configure
static IP for work. The Cisco is configured to issue the IP address dynamically
using the built in DHCP server. I am resolving this now because I will be
turning off the Cisco wireless network (the Omnia will take care of that,
the Cisco is being turned into just a modem) and I wouldn’t want to be locked
out of the administrative interface half-way through the reconfiguration!

Switching the Windows network interface settings to obtain the IP address
dynamically fixes the issue while preserving my work static values conveniently
in the now disabled static section.

[ ] Add the Windows network interface settings dialog modal window screenshot

I am not sure why I am not getting Internet connectivity when on a wired
connection now, but I can access the administrative interface of the Cisco and
that is good enough for me!

The Cisco administrative interface has too many layers to it, I’ll call these
areas, tabs, sections and groups hierarchically and title the following headers
with the path to the relevant group that the heading is dealing with.

**Setup area, Quick Setup tab, Change password section**

The Cisco has some seriously bullshit rules about passphrase complexity (or lack
thereof rather!), but it’s still possible to set up a pretty solid passphrase,
so that’s what I did here.

**Setup area, Quick Setup tab, WLAN section**

I have disabled the wireless network here and cleared the SSID and wireless
security mode fields to make everything nice and tidy. No unused values allowed!

Disabling the wireless network support on the Cisco is one of the more crucial
points of this setup. Aside from the general point of treating the Cisco as a
modem only, this also has security implications, because it limits the attack
surface of the Cisco to attacks that can be carried out with physical access to
the device. The wireless radio is just *off*.

[ ] Add information about KRACK and Omnia update vs Cisco nopedate

**Setup area, Lan Setup tab, Network Setup (LAN) section, Gateway IP group**

*Local IP Address* remains the same at the default value of `192.168.1.1`.
This is a value that I will use for gateway IP when configuring the static IP
connection from the Omnia to the Cisco.

*Subnet Mask* also remains the same at the default value of `255.255.255.0`.
This value will also be used in the Omnia static IP settings.

**Setup area, Lan Setup tab, Network Setup (LAN) section, Network Address Server Settings (DHCP) group**

I am disabling DHCP on the Cisco because I want to configure the Omnia to
connect using a static IP. This will make it a little harder for me to set up,
but similarly to disabling the wireless radio on the Cisco, this server a
purpose of limiting the attack surface to the lowest possible minimum I can
achieve with my current experience level.

Configuring it so that the Omnia connect using a static IP means an attacker
would have to either figure out the static IP configuration to be able to
connect a different device to the Cisco or reset the Cisco to its default
configuration where it uses DHCP to be able to connect their own potentially
malicious device and have it see the local network. To reset the Cisco they
would need physical access thanks to disabling wireless radio as well as remote
administration on the Cisco.

As a part of disabling the DHCP server, I am clearing the pre-assigned IP
addresses, which I’ve used a few of so I could memorize local network IP
addresses of some of the devices on my home network and have that knowledge be
transferable over the Cisco’s/devices’ restarts.

I am not yet sure what specific static IP address I should use with the Omnia.

[ ] Figure out the static IP address to use with the Omnia

I have set the *Maximum Number of DHCP Users* to zero, again, no unused values!
I have never configured LAN 1 static DNS IP addresses, so those all stay at
`0.0.0.0`.

**Setup area, Lan setup tab, Network Setup (LAN) section, Time Settings group**

This is configured with some default time servers, but I’m just going to clear
all those, I don’t want any non-zero unused values, remember!

[ ] Document clearing this section

**Setup area, DDNS tab, DDNS section, DDNS Service group**

[ ] Do this

**Wireless area, WPS tab, Wi-Fi Protected SetupTM section**

[ ] Do this

**Wireless area, WPS tab, Wi-Fi Protected SetupTM section, Wireless Network group**

**Todo**

[ ] Reconnect Omnia
[ ] Set up different basic and advanced admin passwords
[ ] Set up a hidden SSID wireless network for internal networking
[ ] Set up a guest network named using the black hole emoji
[ ] Set up the VPN
[ ] Connect Raspberry Pi 0W to the networked wireless network
